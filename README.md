datalife_office_product
=======================

The office_product module of the Tryton application platform.

[![Build Status](http://drone.datalifeit.es:8050/api/badges/datalifeit/trytond-office_product/status.svg)](http://drone.datalifeit.es:8050/datalifeit/trytond-office_product)

Installing
----------

See INSTALL


License
-------

See LICENSE

Copyright
---------

See COPYRIGHT
